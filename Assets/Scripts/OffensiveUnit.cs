﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffensiveUnit : Unit
{
    [SerializeField]
    protected TargetTactic m_targetPriority;
    [SerializeField]
    protected float m_attackStrength, m_attackStrengthMod;
    [SerializeField]
    protected float m_attackRange, m_attackRangeMod;
    [SerializeField]
    protected float m_attackRate, m_attackRateMod;
    protected float m_attackTimer;

    protected Unit m_currentTarget;

    protected List<Unit> m_targetsInRange =
        new List<Unit>();

    [SerializeField]
    protected UnityEngine.Events.UnityEvent onAttack;

    public bool HasTarget()
    {
        return m_currentTarget != null;
    }

    public Unit CurrentTarget()
    {
        return m_currentTarget;
    }

    public float GetAttackStrength() => m_attackStrength * m_attackStrengthMod;

    // Start is called before the first frame update
    new protected void Start()
    {
        base.Start();
        if(m_navMeshAgent != null)
            m_navMeshAgent.stoppingDistance = m_attackRange * 0.9f;
        m_attackTimer = Random.Range(0f, 1f);
    }

    // Update is called once per frame
    protected void Update()
    {
        m_targetsInRange.RemoveAll(item => item == null);

        if (AttackIsReady())
            AttackCurrentTarget();
    }

    protected bool AttackIsReady()
    {
        m_attackTimer += Time.deltaTime * (m_attackRate * m_attackRateMod);

        if (m_currentTarget == null)
        {
            m_currentTarget = GetNextTarget();

            if (m_currentTarget == null)
                return false;
        }

        if (m_currentTarget.IsDead())
        {
            m_targetsInRange.Remove(m_currentTarget);
            m_currentTarget = null;
            return false;
        }

        GameObject targetObject = (m_currentTarget.gameObject);

        if (targetObject == null)
        {
            m_targetsInRange.Remove(m_currentTarget);
            return false;
        }

        var targetPos = targetObject.transform.position;
        float dist = Vector3.Distance(transform.position, targetPos);

        //RaycastHit rayHit;
        //bool inrange = Physics.Raycast(transform.position, targetPos - transform.position, out rayHit, m_attackRange);

        if (dist < m_attackRange)
        {
            if (m_attackTimer >= 1f)
            {
                m_attackTimer = 0f;
                return true;
            }
        }

        return false;
    }

    protected void AttackCurrentTarget()
    {
        if (m_currentTarget.IsDead())
            return;

        onAttack.Invoke();
        m_currentTarget.TakeDamage(m_attackStrength * m_attackStrengthMod);
    }

    protected void OnTriggerEnter(Collider other)
    {
        Unit target = other.gameObject.GetComponent<Unit>();
        if (target == null || gameObject.CompareTag(other.tag) || m_targetsInRange.Contains(target))
            return;

        m_targetsInRange.Add(target);
    }

    protected void OnTriggerExit(Collider other)
    {
        Unit target = other.GetComponent<Unit>();

        if (target == null || !target.enabled || !m_targetsInRange.Contains(target))
            return;

        m_targetsInRange.Remove(target);
    }

    protected Unit GetNextTarget()
    {
        if (m_targetsInRange.Count <= 0)
            return null;

        var target = m_targetsInRange[0];

        if(m_navMeshAgent != null && m_navMeshAgent.enabled)
            SetDestination(target.transform.position);

        return target;
    }

    public enum TargetTactic
    {
        DEFAULT,
        LOWEST_HEALTH,
        HIGHEST_HEALTH,
        LOWEST_MAX,
        HIGHEST_MAX,
        CLOSEST,
        FARTHEST
    }
}
