﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitHealthDisplay : MonoBehaviour
{
    public Unit m_unit;
    public TMPro.TextMeshPro m_text;

    void Update()
    {
        float remainingHealth = m_unit.GetHealth();
        m_text.text = remainingHealth.ToString();
        m_text.color = remainingHealth <= 4 ? Color.red : Color.green;

        Vector3 camPos = Camera.main.transform.position;
        camPos.y = transform.position.y;
        transform.LookAt(camPos);
    }
}
