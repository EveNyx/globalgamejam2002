﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeUnit : OffensiveUnit
{    
    new protected void Update()
    { 
        base.Update();
        UpdateAnimatorValues();
    }

    void UpdateAnimatorValues()
    {
        GetComponent<Animator>().SetFloat("Speed", m_navMeshAgent.velocity.magnitude);
    }

}
