﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnDelayEvent : MonoBehaviour
{
    public float delayTime;
    public UnityEngine.Events.UnityEvent onTimeExpire;
    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(delayTime);
        onTimeExpire.Invoke();
    }

    public void SelfDestruct()
    {
        Destroy(gameObject);
    }
}
