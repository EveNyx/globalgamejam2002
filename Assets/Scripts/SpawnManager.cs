﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] m_enemies;

    [SerializeField]
    private float m_spawnRate;
    private float m_spawnTimer;
    [SerializeField]
    private uint m_maxSpawnCount;

    protected List<OffensiveUnit> m_spawnedTargets = 
        new List<OffensiveUnit>();
    private int m_curSpawnedTargetIndex;

    Dictionary<Unit, List<OffensiveUnit>> m_attackersOnTowersMap =
        new Dictionary<Unit, List<OffensiveUnit>>();

    [SerializeField]
    private int m_maxAttackersPerTower;

    [SerializeField]
    Vector3 m_basePos = new Vector3(45f, 0f, 0f);
    [SerializeField]
    Vector3 m_enemyStartPos = new Vector3(-45f, 1f, 0f);

    // Start is called before the first frame update
    void Start()
    {
        m_spawnRate = 1;
        m_maxSpawnCount = 50;
        m_curSpawnedTargetIndex = 0;
        m_maxAttackersPerTower = 6;
    }

    // Update is called once per frame
    void Update()
    {
        m_spawnTimer += Time.deltaTime * m_spawnRate * (Mathf.PerlinNoise(Time.time, Time.time) * 4f);

        m_spawnedTargets.RemoveAll(item => item.IsDead() || item == null);
        foreach (var item in m_attackersOnTowersMap)
        {
            item.Value.RemoveAll(x => x.IsDead() || x == null);
        }

        if (m_curSpawnedTargetIndex >= m_spawnedTargets.Count)
            m_curSpawnedTargetIndex = 0;

        if(m_spawnedTargets.Count > 0)
        {
            var enemy = m_spawnedTargets[m_curSpawnedTargetIndex];

            ++m_curSpawnedTargetIndex;

            if(enemy.IsDead() || enemy == null)
            {
                foreach (var item in m_attackersOnTowersMap)
                {
                    if (item.Value.Contains(enemy))
                        item.Value.Remove(enemy);
                }
            }
            else if (!enemy.HasTarget())
            {
                foreach(var item in m_attackersOnTowersMap)
                {
                    if (item.Value.Contains(enemy))
                        item.Value.Remove(enemy);
                }
                enemy.SetDestination(m_basePos);
            }
            else
            {
                var enemyTarget = enemy.CurrentTarget();

                if (!m_attackersOnTowersMap.ContainsKey(enemyTarget))
                {
                    List<OffensiveUnit> unitsOnTower = new List<OffensiveUnit>();
                    m_attackersOnTowersMap.Add(enemyTarget, unitsOnTower);
                }
                if (!m_attackersOnTowersMap[enemyTarget].Contains(enemy)) //todo Might need to check for dead enemies and remove them?
                {
                    if (m_attackersOnTowersMap[enemyTarget].Count >= m_maxAttackersPerTower)
                        enemy.SetDestination(m_basePos);
                    else
                    {
                        enemy.SetDestination(enemyTarget.transform.position);
                        m_attackersOnTowersMap[enemyTarget].Add(enemy);
                    }
                }
            }
        }


        if (m_spawnTimer < 1f)
            return;

        if (m_spawnedTargets.Count > m_maxSpawnCount)
            return;

        StartCoroutine(NewSpawnSetUp());
        m_spawnTimer = 0f;
    }

    private IEnumerator NewSpawnSetUp()
    { 
        GameObject newSpawn = Instantiate(m_enemies[Random.Range(0, m_enemies.Length)], m_enemyStartPos, Quaternion.identity);
        OffensiveUnit newEnemy = newSpawn.GetComponent<OffensiveUnit>();
        m_spawnedTargets.Add(newEnemy);

        yield return new WaitForEndOfFrame();
        newEnemy.SetDestination(m_basePos);
    }

    public void IncreaseSpawnRate(float mod)
    {
        m_spawnRate += mod;
    }
}
