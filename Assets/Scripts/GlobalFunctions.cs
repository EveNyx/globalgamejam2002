﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalFunctions : MonoBehaviour
{
    private static GlobalFunctions _instance;
    public static GlobalFunctions instance => _instance;

    Mouledoux.Components.Mediator.Subscriptions subscriptions =
        new Mouledoux.Components.Mediator.Subscriptions();

    void Awake()
    {
        if (instance == null)
        {
            _instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);

        subscriptions.Subscribe("hidemouse", HideMouse);
        subscriptions.Subscribe("restart", RestartScene);
        subscriptions.Subscribe("quit", QuitGame);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            RestartScene(null);
    }

    private void RestartScene(object[] args)
    {
        ShowMouse(null);
        UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }

    private void QuitGame(object[] args)
    {
        Application.Quit();
    }

    private void HideMouse(object args)
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    private void ShowMouse(object args)
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
}
