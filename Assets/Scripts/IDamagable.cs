﻿using System.Collections;
using System.Collections.Generic;

namespace interfaces
{
    public interface IDamagable
    {
        float GetHealth();
        bool IsDead();
        float TakeDamage(float damage);
    }
} // interfaces
