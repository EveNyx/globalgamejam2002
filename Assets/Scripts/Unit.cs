﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Unit : MonoBehaviour, interfaces.IDamagable
{
    [SerializeField]
    protected float m_maxHealth;
    [SerializeField]
    protected float m_currentHealth;
    [SerializeField]
    protected float m_initialMovememntSpeed;
    [SerializeField]
    protected float m_currentMovementSpeed;
    [SerializeField]
    protected UnityEngine.Events.UnityEvent onDie;

    protected UnityEngine.AI.NavMeshAgent m_navMeshAgent;


    protected void Start()
    {
        m_currentHealth = m_maxHealth;
        m_currentMovementSpeed = m_initialMovememntSpeed;
        
        m_navMeshAgent = GetComponent<UnityEngine.AI.NavMeshAgent>();

        if (m_navMeshAgent != null)
            m_navMeshAgent.speed = m_currentMovementSpeed;
    }

    public float GetHealth()
    {
        return m_currentHealth;
    }

    public bool IsDead()
    {
        return m_currentHealth <= 0;
    }

    public float TakeDamage(float damage)
    {
        m_currentHealth -= damage;

        if (m_currentHealth <= 0)
            Die();

        return m_currentHealth;
    }

    public void SetDestination(Vector3 destination)
    {
        m_navMeshAgent.SetDestination(destination);
    }

    protected virtual void Die ()
    {
        onDie.Invoke();
    }

    public void Despawn()
    {
        Destroy(gameObject);
    }
}