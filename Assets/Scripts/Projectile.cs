﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : OffensiveUnit
{
    [SerializeField]
    protected GameObject m_hitFX;
    // Update is called once per frame
    new void Update()
    {
        transform.Translate(Vector3.forward * m_currentMovementSpeed * Time.deltaTime);
    }

    new protected void OnTriggerEnter(Collider other)
    {
        //Collider target = other.collider
        base.OnTriggerEnter(other);

        if (base.AttackIsReady())
        {
            base.AttackCurrentTarget();
        }

        Instantiate(m_hitFX, transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
