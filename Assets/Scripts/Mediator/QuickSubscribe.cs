﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickSubscribe : MonoBehaviour
{
    public List<QuickSub> quickSubs = new List<QuickSub>();

    private Mouledoux.Components.Mediator.Subscriptions subscriptions =
        new Mouledoux.Components.Mediator.Subscriptions();

    private void Awake()
    {
        foreach(QuickSub q in quickSubs)
        {
            subscriptions.Subscribe(q.mesasge, (object[] x) => q.onMessage.Invoke());
        }
    }
}

[System.Serializable]
public class QuickSub
{
    public string mesasge;
    public UnityEngine.Events.UnityEvent onMessage;
}
