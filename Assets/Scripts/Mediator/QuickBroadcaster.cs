﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickBroadcaster : MonoBehaviour
{
    public void QuickBroadcast(string message)
    {
        Mouledoux.Components.Mediator.NotifySubscribers(message);
    }
}
