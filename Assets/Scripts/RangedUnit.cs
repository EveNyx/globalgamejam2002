﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangedUnit : OffensiveUnit
{
    [SerializeField]
    protected uint m_ammoCount;
    [SerializeField]
    protected uint m_maxAmmoCount = 50;
    [SerializeField]
    protected GameObject m_ammoType;

    [SerializeField]
    protected Transform m_ammoSpawnPoint;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();

        m_ammoCount = m_maxAmmoCount;
    }

    new void AttackCurrentTarget()
    {
        if (m_currentTarget == null || m_currentTarget.IsDead())
            return;

        LaunchProjectileAtCurrentTarget();
    }

    void LaunchProjectileAtCurrentTarget()
    {
        if (m_ammoCount <= 0)
            return;

        var enemyPos = m_currentTarget.transform.position + m_currentTarget.transform.forward;
        var launchVec = enemyPos - m_ammoSpawnPoint.position;

        GameObject ammoInstance =
            Instantiate(m_ammoType, m_ammoSpawnPoint.position, Quaternion.identity);
        ammoInstance.tag = gameObject.tag;
        ammoInstance.transform.forward = launchVec;

        --m_ammoCount;

        return;
    }

    // Update is called once per frame
    new void Update()
    {
        m_targetsInRange.RemoveAll(item => item == null);

        if (base.AttackIsReady())
            AttackCurrentTarget();
    }

    private void FixedUpdate()
    {
        if(m_attackRateMod > 1f)
        {
            m_attackRateMod -= Time.deltaTime;
        }
    }

    new protected void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);

        OffensiveUnit target = other.gameObject.GetComponent<OffensiveUnit>();
        if (target == null || !target.CompareTag(tag))
            return;

        if (m_currentHealth < m_maxHealth)
            TakeDamage(-target.GetAttackStrength());
        else if(m_attackRateMod < 8f)
            m_attackRateMod += .8f;
    }
}
